require 'rails_helper'

RSpec.describe BootstrapTimelineService, type: :service do
  subject { described_class.new }

  describe '#call' do
    let(:source_account) { Fabricate(:account) }

    context 'when setting is set' do
      let!(:alice) { Fabricate(:account, username: 'alice') }
      let!(:bob)   { Fabricate(:account, username: 'bob') }

      before do
        Setting.bootstrap_timeline_accounts = 'alice, bob'
        subject.call(source_account)
      end

      it 'follows found accounts from account' do
        expect(source_account.following?(alice)).to be true
        expect(source_account.following?(bob)).to be true
      end
    end
  end
end
