# Spinster

Spinster is an open source and fully decentralized social network. It is the social network of spinster.xyz. By offering decentralization and federation, Spinster demonstrates a lack of interest in the ownership of your data.

The Spinster project is based on [Gab Social](https://code.gab.com/gab/social/gab-social), which is in turn based on the [Mastodon](https://github.com/tootsuite/mastodon) project. It is licensed under the terms and conditions of AGPL-3.0.

## Project goals

We have diverged from Mastodon in several ways in pursuit of our own goals.

1. Node.js has been updated to 10.15.3LTS for hosting the Streaming API.
1. Statuses were renamed from 'toots' to 'gabs'
1. The maximum length of a status was increased to 3,000 characters
1. Advanced media (MP4, WebM, etc.) was limited to PRO subscribers
1. The creation of custom emoji was limited to PRO subscribers
1. The browser client user experience has been significantly altered to match what users of Spinster will expect

## BTCPay
In order to make BTC flow work, 3 enviornment variables need to be set:

- `BTCPAY_LEGACY_TOKEN`: So called Legacy Tokens can be found in https://btcpay.xxx.com/stores/yyy/Tokens
- `BTCPAY_PUB_KEY`: Public key that is used when creating an access token or pairing https://btcpay.xxx.com/stores/yyy/Tokens/Create
- `BTCPAY_MERCHANT_TOKEN`: Token created for facade *merchant*

## Deployment

**Tech stack:**

- **Ruby on Rails** powers the REST API and other web pages
- **React.js** and Redux are used for the dynamic parts of the interface
- **Node.js** powers the streaming API

**Requirements:**

- **PostgreSQL** 9.5+
- **Redis**
- **Ruby** 2.4+
- **Node.js** 8+

The repository includes deployment configurations for **Docker and docker-compose**, but also a few specific platforms like **Heroku**, **Scalingo**, and **Nanobox**.

A **stand-alone** installation guide will be provided as soon as possible.

## Local development

To get started developing on Spinster, you will need to run a version of it locally.
The following instructions assume you are already familiar with using a terminal program.

1. Install [Vagrant](https://www.vagrantup.com/) and [VirtualBox](https://www.virtualbox.org/) if you haven't already.
2. Clone this repository with `git clone https://gitlab.com/spinster-xyz/spinster.git`
3. Change into the project directory with `cd spinster`
4. Run `vagrant up` to provision the virtual machine. This will take a while.
5. Finally, run `vagrant ssh -c "cd /vagrant && foreman start"` to start the local web server.
6. Visit http://0.0.0.0:3000 in your web browser to see Spinster's splash screen. If it doesn't load, or styling is missing, wait another minute and refresh the page.
7. Log in with the username `admin` and password `administrator`
8. Have fun developing on Spinster!

## License

Copyright (C) 2019 Spinster LLC, and other contributors (see [AUTHORS.md](AUTHORS.md))

Copyright (C) 2019 Gab AI, Inc., and other contributors

Copyright (C) 2016-2019 Eugen Rochko & other Mastodon contributors

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
